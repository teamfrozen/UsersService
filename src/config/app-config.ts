import {get} from 'env-var';

export class AppConfig {
  public static readonly APP_PORT: number = get('APP_PORT').required().asPortNumber();
  public static readonly APP_SECRET: string = get('APP_SECRET').required().asString();
  public static readonly APP_HASH_SECRET: string = get('APP_HASH_SECRET')
    .required()
    .asString();
  public static readonly APP_API_KEY: string = get('APP_API_KEY').required().asString();
  public static readonly APP_SWAGGER: boolean = get('APP_SWAGGER').required().asBool();
}
