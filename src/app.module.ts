import {Module} from '@nestjs/common';
import {StatusModule} from './status/status.module';
import {DatabaseModule} from './database/database.module';
import {CryptoModule} from './crypto/crypto.module';
import {LoggerModule} from './logger/logger.module';
import {UsersModule} from './users/users.module';
import {AuthModule} from './auth/auth.module';

@Module({
  imports: [
    // prettier-ignore
    LoggerModule,
    DatabaseModule,
    CryptoModule,
    UsersModule,
    AuthModule,
    StatusModule,
  ],
})
export class AppModule {}
