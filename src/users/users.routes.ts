export enum UsersControllerRoutes {
  Root = '/api/v1/users',
  GetUsersList = '/',
  CreateUserOrIgnore = '/',
  GetUserByIdOrFail = '/:id',
  GetUserByTelegramIdOrFail = '/telegram/:telegramId',
  UpdateUserOrFail = '/:id',
  RemoveUserOrFail = '/:id',
}
