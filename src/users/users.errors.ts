import {CustomError} from '../common/error-custom';

export class UserAlreadyExists extends CustomError {
  constructor() {
    super('USER_ALREADY_EXISTS');
  }
}

export class UserDoesntExist extends CustomError {
  constructor() {
    super('USER_DOESNT_EXIST');
  }
}
