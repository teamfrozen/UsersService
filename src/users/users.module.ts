import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from './entities/user.entity';
import {UsersController} from './users.controller';
import {UsersRepository} from './users.repository';
import {UsersService} from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, UsersRepository])],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService, TypeOrmModule],
})
export class UsersModule {}
