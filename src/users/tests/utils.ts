import {TestCtx} from '../../utils/tests-setup';
import {getManager} from 'typeorm';
import {CryptoService} from '../../crypto/crypto.service';
import {User} from '../entities/user.entity';
import {UsersControllerRoutes} from '../users.routes';
import {AppConfig} from '../../config/app-config';
import {replaceParam} from '../../utils/string';

const crypto = new CryptoService();

export function createUser(user: Partial<User> = {}): Promise<User> {
  return getManager()
    .getRepository(User)
    .save({
      login: 'test',
      firstName: 'test',
      passwordHash: crypto.createHash('test'),
      ...user,
    });
}

export function createUsersApi(ctx: TestCtx) {
  return {
    getUsersList({take, skip}: {take?: number; skip?: number} = {}): Promise<User[]> {
      return ctx.agent
        .get(UsersControllerRoutes.Root + UsersControllerRoutes.GetUsersList)
        .query({take, skip})
        .set({apikey: AppConfig.APP_API_KEY})
        .then((res) => res.body);
    },
    getUserByIdOrFail(id: string): Promise<User> {
      return ctx.agent
        .get(
          UsersControllerRoutes.Root +
            replaceParam(UsersControllerRoutes.GetUserByIdOrFail, id),
        )
        .set({apikey: AppConfig.APP_API_KEY})
        .then((res) => res.body);
    },
    createUserOrFail(login: string, password: string, firstName: string): Promise<User> {
      return ctx.agent
        .post(UsersControllerRoutes.Root + UsersControllerRoutes.CreateUserOrIgnore)
        .send({login, password, firstName})
        .set({apikey: AppConfig.APP_API_KEY})
        .then((res) => res.body);
    },
    updateUserOrFail(id: string, data: Partial<User> = {}) {
      return ctx.agent
        .patch(
          UsersControllerRoutes.Root +
            replaceParam(UsersControllerRoutes.UpdateUserOrFail, id),
        )
        .send(data)
        .set({apikey: AppConfig.APP_API_KEY})
        .then((res) => res.body);
    },
    removeUserOrFail(id: string): Promise<void> {
      return ctx.agent
        .delete(
          UsersControllerRoutes.Root +
            replaceParam(UsersControllerRoutes.RemoveUserOrFail, id),
        )
        .set({apikey: AppConfig.APP_API_KEY})
        .then((res) => res.body);
    },
  };
}
