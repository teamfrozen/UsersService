import {maskObject} from '../../utils/test-mask-object';
import {synchronize} from '../../utils/test-synchronize';
import {setup, TestCtx} from '../../utils/tests-setup';
import {teardown} from '../../utils/tests-teardown';
import {createUser} from './utils';
import {createUsersApi} from './utils';
import {v4 as uuid} from 'uuid';

let ctx: TestCtx;

let api: ReturnType<typeof createUsersApi>;

const mask = <T>(obj: T) =>
  maskObject(obj, ['id', 'createdAt', 'updatedAt', 'timestamp', 'path'], {levels: 'all'});

beforeAll(async () => {
  ctx = await setup();
  api = createUsersApi(ctx);
});

afterEach(async () => {
  await synchronize(ctx.app);
});

afterAll(async () => {
  await teardown(ctx.app);
});

describe('UsersController (e2e)', () => {
  describe('GetUsersList', () => {
    test('Get list of users', async () => {
      await Promise.all([createUser({login: 'test1'}), createUser({login: 'test2'})]);

      await expect(api.getUsersList().then(mask)).resolves.toMatchSnapshot();
    });

    test('Get list of users and take', async () => {
      await Promise.all([createUser({login: 'test1'}), createUser({login: 'test2'})]);

      await expect(api.getUsersList({take: 1}).then(mask)).resolves.toMatchSnapshot();
    });

    test('Get list of users and limit', async () => {
      await Promise.all([createUser({login: 'test1'}), createUser({login: 'test2'})]);

      await expect(api.getUsersList({skip: 1}).then(mask)).resolves.toMatchSnapshot();
    });
  });

  describe('GetUserByIdOrFail', () => {
    test('Get user by id if user exists', async () => {
      const [user1, user2] = await Promise.all([
        createUser({login: 'test1'}),
        createUser({login: 'test2'}),
      ]);

      await expect(api.getUserByIdOrFail(user1.id).then(mask)).resolves.toMatchSnapshot();
      await expect(api.getUserByIdOrFail(user2.id).then(mask)).resolves.toMatchSnapshot();
    });

    test('Get error if user doesnt exist', async () => {
      await Promise.all([createUser({login: 'test1'}), createUser({login: 'test2'})]);

      await expect(api.getUserByIdOrFail(uuid()).then(mask)).resolves.toMatchSnapshot();
    });
  });

  describe('CreateUserOrFail', () => {
    test('Create user if doesnt exist', async () => {
      await expect(
        api.createUserOrFail('test', 'test', 'test').then(mask),
      ).resolves.toMatchSnapshot();
    });

    test('Get error if user already exists', async () => {
      await createUser({login: 'test'});

      await expect(
        api.createUserOrFail('test', 'test', 'test').then(mask),
      ).resolves.toMatchSnapshot();
    });
  });

  describe('updateUserOrFail', () => {
    test('Update user if user exists', async () => {
      const user = await createUser({login: 'test'});
      const newUserData = {firstName: 'hello world'};

      await expect(
        api.updateUserOrFail(user.id, newUserData).then(mask),
      ).resolves.toMatchSnapshot();
      await expect(api.getUserByIdOrFail(user.id).then(mask)).resolves.toMatchSnapshot();
    });

    test('Get error if user already exists', async () => {
      await expect(api.updateUserOrFail(uuid()).then(mask)).resolves.toMatchSnapshot();
    });
  });

  describe('RemoveUserOrFail', () => {
    test('Remove user if user exists', async () => {
      const user = await createUser({login: 'test'});
      await expect(api.removeUserOrFail(user.id).then(mask)).resolves.toMatchSnapshot();
      await expect(api.getUserByIdOrFail(user.id).then(mask)).resolves.toMatchSnapshot();
    });

    test('Get error if usre doesnt exist', async () => {
      await expect(api.removeUserOrFail(uuid()).then(mask)).resolves.toMatchSnapshot();
    });
  });
});
