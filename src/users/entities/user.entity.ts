import {ApiProperty} from '@nestjs/swagger';
import {IsOptional, IsString, IsUUID} from 'class-validator';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({name: 'users'})
export class User {
  @ApiProperty()
  @IsOptional()
  @IsUUID()
  @Generated('uuid')
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty()
  @IsString()
  @Column({nullable: true})
  login?: string;

  @ApiProperty()
  @IsString()
  @Column({nullable: true})
  firstName?: string;

  @ApiProperty()
  @IsString()
  @Column({nullable: true, unique: true})
  telegramId?: string;

  @ApiProperty()
  @IsString()
  @Column({nullable: true})
  passwordHash?: string;

  @ApiProperty()
  @IsOptional()
  @CreateDateColumn({type: 'timestamp without time zone'})
  createdAt!: Date;

  @ApiProperty()
  @IsOptional()
  @UpdateDateColumn({type: 'timestamp without time zone'})
  updatedAt!: Date;
}
