import {ApiOkResponse, ApiOperation, ApiSecurity, ApiTags} from '@nestjs/swagger';
import {GetUsersListQuery, GetUsersListResponse} from './dto/get-users-list.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  GetUserByIdOrFailParam,
  GetUserByIdOrFailResponse,
} from './dto/get-user-by-id-or-fail.dto';
import {
  UpdateUserOrFailBody,
  UpdateUserOrFailParam,
  UpdateUserOrFailResponse,
} from './dto/update-user-or-fail.dto';
import {
  RemoveUserOrFailParam,
  RemoveUserOrFailResponse,
} from './dto/remove-user-or-fail.dto';
import {
  CreateUserOrFailBody,
  CreateUserOrFailResponse,
} from './dto/create-user-or-fail.dto';
import {UsersControllerRoutes} from './users.routes';
import {UsersService} from './users.service';
import {ApiKeyGuard} from '../auth/strategies/api-key/api-key.guard';
import {AuthStrategy} from '../auth/auth.constants';
import {
  GetUserByTelegramIdIdOrFailParam,
  GetUserByTelegramIdResponse,
} from './dto/get-user-by-telegram-id-or-fail.dto';

@ApiTags('Users')
@Controller(UsersControllerRoutes.Root)
@ApiSecurity(AuthStrategy.ApiKey)
@UseGuards(ApiKeyGuard)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get(UsersControllerRoutes.GetUsersList)
  @ApiOkResponse({type: GetUsersListResponse})
  @ApiOperation({
    operationId: 'getUsersList',
    summary: 'Get list of users',
  })
  public async getUsersList(
    @Query() {take, skip}: GetUsersListQuery,
  ): Promise<GetUsersListResponse> {
    const payload = await this.usersService.getUsersList(take, skip);

    return {status: 'ok', payload};
  }

  @Get(UsersControllerRoutes.GetUserByTelegramIdOrFail)
  @ApiOkResponse({type: GetUserByTelegramIdResponse})
  @ApiOperation({
    operationId: 'getUserByTelegramIdOrFail',
    summary: 'Get user by telegramId or fail',
  })
  public async getUserByTelegramIdOrFail(
    @Param() {telegramId}: GetUserByTelegramIdIdOrFailParam,
  ): Promise<GetUserByTelegramIdResponse> {
    const payload = await this.usersService.getUserByTelegramIdOrFail(telegramId);

    return {status: 'ok', payload};
  }

  @Get(UsersControllerRoutes.GetUserByIdOrFail)
  @ApiOkResponse({type: GetUserByIdOrFailResponse})
  @ApiOperation({
    operationId: 'getUserByIdOrFail',
    summary: 'Get user by id or fail',
  })
  public async getUserByIdOrFail(
    @Param() {id}: GetUserByIdOrFailParam,
  ): Promise<GetUserByIdOrFailResponse> {
    const payload = await this.usersService.getUserByIdOrFail(id);

    return {status: 'ok', payload};
  }

  @Post(UsersControllerRoutes.CreateUserOrIgnore)
  @ApiOkResponse({type: CreateUserOrFailResponse})
  @ApiOperation({
    operationId: 'createUserOrIgnore',
    summary: 'Create user or ignore',
  })
  public async createUserOrFail(
    @Body() user: CreateUserOrFailBody,
  ): Promise<CreateUserOrFailResponse> {
    const payload = await this.usersService.createOrIgnore(user);

    return {status: 'ok', payload};
  }

  @Patch(UsersControllerRoutes.UpdateUserOrFail)
  @ApiOkResponse({type: UpdateUserOrFailResponse})
  @ApiOperation({
    operationId: 'updateUserOrFail',
    summary: 'Update user or fail',
  })
  public async updateUserOrFail(
    @Param() {id}: UpdateUserOrFailParam,
    @Body() data: UpdateUserOrFailBody,
  ): Promise<UpdateUserOrFailResponse> {
    const payload = await this.usersService.updateUserOrFail(id, data);

    return {status: 'ok', payload};
  }

  @Delete(UsersControllerRoutes.RemoveUserOrFail)
  @ApiOkResponse({type: RemoveUserOrFailResponse})
  @ApiOperation({
    operationId: 'removeUserOrFail',
    summary: 'Remove user or fail',
  })
  public async removeUserOrFail(
    @Param() {id}: RemoveUserOrFailParam,
  ): Promise<RemoveUserOrFailResponse> {
    await this.usersService.removeUserOrFail(id);

    return {status: 'ok'};
  }
}
