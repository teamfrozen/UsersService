import {CryptoService} from '../crypto/crypto.service';
import {UsersRepository} from './users.repository';
import {User} from './entities/user.entity';
import {FindManyOptions} from 'typeorm';
import {Injectable} from '@nestjs/common';

export interface CreateUserParams {
  firstName?: string;
  password?: string;
  login?: string;
  telegramId?: string;
}

@Injectable()
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly cryptoService: CryptoService,
  ) {}

  public async createOrIgnore(user: CreateUserParams): Promise<User> {
    const userEntity = await this.usersRepository.findUserByTelegramId(user.telegramId);
    if (userEntity) {
      return userEntity;
    }

    const insertResult = await this.usersRepository.createOrIgnore(
      this.mapToPartialUser(user),
    );

    return await this.usersRepository.findUserByIdOrFail(insertResult.identifiers[0].id);
  }

  public async createUserOrFail(user: CreateUserParams): Promise<User> {
    return await this.usersRepository.createOrFail(this.mapToPartialUser(user));
  }

  public async getUsersList(take: number, skip: number): Promise<User[]> {
    const query: FindManyOptions<User> = {
      order: {login: 'ASC'},
    };

    if (take) query.take = take;
    if (skip) query.skip = skip;

    return await this.usersRepository.find(query);
  }

  public async getUserByIdOrFail(id: string): Promise<User> {
    return await this.usersRepository.findUserByIdOrFail(id);
  }

  public async getUserByLoginOrFail(login: string): Promise<User> {
    return await this.usersRepository.findUserByLoginOrFail(login);
  }

  public async getUserByTelegramIdOrFail(telegramId: string): Promise<User> {
    return await this.usersRepository.findUserByTelegramIdOrFail(telegramId);
  }

  public async updateUserOrFail(
    id: string,
    data: Partial<CreateUserParams>,
  ): Promise<User> {
    return await this.usersRepository.updateUserOrFail(id, this.mapToPartialUser(data));
  }

  public async removeUserOrFail(id: string): Promise<void> {
    await this.usersRepository.removeUserOrFail(id);
  }

  public mapToPartialUser(data: Partial<CreateUserParams>): Partial<User> {
    const user: Partial<User> = {};

    if (data.password) {
      user.passwordHash = this.cryptoService.createHash(data.password);
    }

    return {
      ...data,
      ...user,
    };
  }
}
