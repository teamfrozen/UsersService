import {ApiProperty} from '@nestjs/swagger';
import {SuccessResponse} from '../../common/response-success';
import {IsOptional, IsString, IsUUID} from 'class-validator';
import {User} from '../entities/user.entity';

export class UpdateUserOrFailBody {
  @ApiProperty()
  @IsOptional()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  firstName!: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  password!: string;
}

export class UpdateUserOrFailParam {
  @ApiProperty()
  @IsUUID()
  id!: string;
}

export class UpdateUserOrFailResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}
