import {ApiProperty} from '@nestjs/swagger';
import {IsUUID} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../entities/user.entity';

export class GetUserByIdOrFailParam {
  @ApiProperty()
  @IsUUID()
  id!: string;
}

export class GetUserByIdOrFailResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}
