import {ApiProperty} from '@nestjs/swagger';
import {IsUUID} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';

export class RemoveUserOrFailResponse extends SuccessResponse {}

export class RemoveUserOrFailParam {
  @ApiProperty()
  @IsUUID()
  id!: string;
}
