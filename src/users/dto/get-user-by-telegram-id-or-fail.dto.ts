import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../entities/user.entity';

export class GetUserByTelegramIdResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}

export class GetUserByTelegramIdIdOrFailParam {
  @ApiProperty()
  @IsString()
  telegramId!: string;
}
