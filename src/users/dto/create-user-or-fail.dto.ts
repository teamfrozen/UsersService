import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../entities/user.entity';

export class CreateUserOrFailBody {
  @ApiProperty()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsString()
  firstName!: string;

  @ApiProperty()
  @IsString()
  password!: string;

  @ApiProperty()
  @IsString()
  telegramId!: string;
}

export class CreateUserOrFailResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}
