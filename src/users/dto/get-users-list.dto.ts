import {ApiProperty} from '@nestjs/swagger';
import {IsNumber} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../entities/user.entity';

export class GetUsersListQuery {
  @ApiProperty()
  @IsNumber()
  take!: number;

  @ApiProperty()
  @IsNumber()
  skip!: number;
}

export class GetUsersListResponse extends SuccessResponse {
  @ApiProperty({type: User, isArray: true})
  payload!: User[];
}
