import {EntityManager, EntityRepository, InsertResult, Repository} from 'typeorm';
import {UserAlreadyExists, UserDoesntExist} from './users.errors';
import {User} from './entities/user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  public async createOrIgnore(
    user: Partial<User>,
    manager?: EntityManager,
  ): Promise<InsertResult> {
    return await this.getRepository(manager)
      .createQueryBuilder()
      .insert()
      .values(user)
      .orIgnore()
      .execute();
  }

  public async createOrFail(user: Partial<User>, manager?: EntityManager): Promise<User> {
    const userEntity = await this.findUserByLogin(user.login);
    if (userEntity) {
      throw new UserAlreadyExists();
    }

    return await this.getRepository(manager).save(user);
  }

  public async findUserById(
    id: string,
    manager?: EntityManager,
  ): Promise<User | undefined> {
    return await this.getRepository(manager).findOne(id);
  }

  public async findUserByLogin(
    login?: string,
    manager?: EntityManager,
  ): Promise<User | undefined> {
    if (login) {
      return await this.getRepository(manager).findOne({where: {login}});
    }
  }

  public async findUserByTelegramId(
    telegramId?: string,
    manager?: EntityManager,
  ): Promise<User | undefined> {
    return await this.getRepository(manager).findOne({where: {telegramId}});
  }

  public async findUserByTelegramIdOrFail(
    telegramId: string,
    manager?: EntityManager,
  ): Promise<User> {
    const user = await this.getRepository(manager).findOne({where: {telegramId}});
    if (!user) {
      throw new UserDoesntExist();
    }

    return user;
  }

  public async findUserByIdOrFail(id: string, manager?: EntityManager): Promise<User> {
    const user = await this.getRepository(manager).findOne(id);
    if (!user) {
      throw new UserDoesntExist();
    }

    return user;
  }

  public async findUserByLoginOrFail(
    login: string,
    manager?: EntityManager,
  ): Promise<User> {
    const user = await this.getRepository(manager).findOne({where: {login}});
    if (!user) {
      throw new UserDoesntExist();
    }

    return user;
  }

  public async removeUserOrFail(id: string, manager?: EntityManager): Promise<void> {
    const user = await this.getRepository(manager).findOne(id);
    if (!user) {
      throw new UserDoesntExist();
    }

    await this.getRepository(manager).delete(id);
  }

  public async updateUserOrFail(
    id: string,
    data: Partial<User>,
    manager?: EntityManager,
  ): Promise<User> {
    const user = await this.getRepository(manager).findOne(id);
    if (!user) {
      throw new UserDoesntExist();
    }

    await this.getRepository(manager).update(id, data);

    return this.findUserByIdOrFail(id);
  }

  private getRepository(manager?: EntityManager) {
    return manager ? manager.getRepository(User) : this;
  }
}
