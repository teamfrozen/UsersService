import {Injectable} from '@nestjs/common';
import {AppConfig} from '../config/app-config';
import crypto from 'crypto';

@Injectable()
export class CryptoService {
  private readonly secret: string;

  constructor() {
    this.secret = AppConfig.APP_HASH_SECRET;
  }

  public createHash(data: string): string {
    return crypto.createHmac('sha256', this.secret).update(data).digest('hex');
  }
}
