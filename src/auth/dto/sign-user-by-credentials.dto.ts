import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../../users/entities/user.entity';

export class SignUserByCredentialsBody {
  @ApiProperty()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsString()
  firstName!: string;

  @ApiProperty()
  @IsString()
  password!: string;
}

export class SignUserByCredentialsResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}
