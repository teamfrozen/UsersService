import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../../users/entities/user.entity';

export class AuthUserByCredentialsResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}

export class AuthUserByCredentialsBody {
  @ApiProperty()
  @IsString()
  login!: string;

  @ApiProperty()
  @IsString()
  password!: string;
}
