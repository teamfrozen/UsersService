import {ApiProperty} from '@nestjs/swagger';
import {IsString} from 'class-validator';
import {SuccessResponse} from '../../common/response-success';
import {User} from '../../users/entities/user.entity';

export class SignUserByTelegramBody {
  @ApiProperty()
  @IsString()
  telegramId!: string;

  @ApiProperty()
  @IsString()
  firstName!: string;
}

export class SignUserByTelegramResponse extends SuccessResponse {
  @ApiProperty({type: User})
  payload!: User;
}
