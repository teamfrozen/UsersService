import {Body, Controller, Post, UseGuards} from '@nestjs/common';
import {ApiOkResponse, ApiOperation, ApiSecurity, ApiTags} from '@nestjs/swagger';
import {
  AuthUserByCredentialsBody,
  AuthUserByCredentialsResponse,
} from './dto/auth-user-by-credentials.dto';
import {
  SignUserByCredentialsBody,
  SignUserByCredentialsResponse,
} from './dto/sign-user-by-credentials.dto';
import {ApiKeyGuard} from './strategies/api-key/api-key.guard';
import {AuthControllerRoutes} from './auth.routes';
import {AuthStrategy} from './auth.constants';
import {AuthService} from './auth.service';
import {
  SignUserByTelegramBody,
  SignUserByTelegramResponse,
} from './dto/sign-user-by-telegram.dto';

@ApiTags('Auth')
@ApiSecurity(AuthStrategy.ApiKey)
@UseGuards(ApiKeyGuard)
@Controller(AuthControllerRoutes.Root)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post(AuthControllerRoutes.AuthUserByCredentials)
  @ApiOkResponse({type: AuthUserByCredentialsResponse})
  @ApiOperation({
    operationId: 'authUserByCredentials',
    summary: 'Authorize user by credentials',
  })
  public async authUserByCredentials(
    @Body() {login, password}: AuthUserByCredentialsBody,
  ): Promise<AuthUserByCredentialsResponse> {
    const payload = await this.authService.authUserByCredentials(login, password);

    return {status: 'ok', payload};
  }

  @Post(AuthControllerRoutes.SignUserByTelegram)
  @ApiOkResponse({type: SignUserByTelegramResponse})
  @ApiOperation({
    operationId: 'signUserByTelegram',
    summary: 'Sign user by telegram',
  })
  public async signUserByTelegram(
    @Body() {telegramId, firstName}: SignUserByTelegramBody,
  ): Promise<SignUserByTelegramResponse> {
    const payload = await this.authService.signUserByTelegram(telegramId, firstName);

    console.log(payload);

    return {status: 'ok', payload};
  }

  @Post(AuthControllerRoutes.SignUserByCredentials)
  @ApiOkResponse({type: SignUserByCredentialsResponse})
  @ApiOperation({
    operationId: 'signUserByCredentials',
    summary: 'Sign user by credentials',
  })
  public async signUserByCredentials(
    @Body() {login, firstName, password}: SignUserByCredentialsBody,
  ): Promise<AuthUserByCredentialsResponse> {
    const payload = await this.authService.signUserByCredentials(
      login,
      password,
      firstName,
    );

    return {status: 'ok', payload};
  }
}
