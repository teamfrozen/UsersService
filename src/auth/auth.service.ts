import {Injectable, UnauthorizedException} from '@nestjs/common';
import {CryptoService} from '../crypto/crypto.service';
import {User} from '../users/entities/user.entity';
import {UsersService} from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly cryptoService: CryptoService,
  ) {}

  public async signUserByCredentials(
    login: string,
    password: string,
    firstName: string,
  ): Promise<User> {
    return await this.usersService.createUserOrFail({login, password, firstName});
  }

  public async signUserByTelegram(telegramId: string, firstName: string): Promise<User> {
    return await this.usersService.createOrIgnore({telegramId, firstName});
  }

  public async authUserByCredentials(login: string, password: string): Promise<User> {
    const user = await this.usersService.getUserByLoginOrFail(login);

    const passwordHash = this.cryptoService.createHash(password);

    if (user.passwordHash !== passwordHash) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
