import {PassportStrategy} from '@nestjs/passport';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {HeaderAPIKeyStrategy} from 'passport-headerapikey';
import {AppConfig} from '../../../config/app-config';
import {AuthStrategy} from '../../auth.constants';

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(
  HeaderAPIKeyStrategy,
  AuthStrategy.ApiKey,
) {
  constructor() {
    super({header: 'ApiKey'}, false, validate());
  }
}

function validate() {
  return (
    apiKey: string,
    done: (error: UnauthorizedException | null, user?: any) => void,
  ) => {
    return apiKey === AppConfig.APP_API_KEY
      ? done(null, true)
      : done(new UnauthorizedException());
  };
}
