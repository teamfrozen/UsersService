import {AuthGuard} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';

import {AuthStrategy} from '../../auth.constants';

@Injectable()
export class ApiKeyGuard extends AuthGuard(AuthStrategy.ApiKey) {}
