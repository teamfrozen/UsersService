import {Module} from '@nestjs/common';
import {JwtModule} from '@nestjs/jwt';
import {AppConfig} from '../config/app-config';
import {UsersModule} from '../users/users.module';
import {AuthController} from './auth.controller';
import {AuthService} from './auth.service';
import {ApiKeyStrategy} from './strategies/api-key/api-key.strategy';

@Module({
  imports: [JwtModule.register({secret: AppConfig.APP_SECRET}), UsersModule],
  providers: [AuthService, ApiKeyStrategy],
  exports: [AuthService, ApiKeyStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
