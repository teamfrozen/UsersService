export enum AuthControllerRoutes {
  Root = '/api/v1/auth',
  SignUserByCredentials = '/sign',
  SignUserByTelegram = '/sign/telegram',
  AuthUserByCredentials = '/login',
}
