import {User} from '../users/entities/user.entity';
import {Request} from 'express';

export type RequestWithUser = Request & {user: User};
