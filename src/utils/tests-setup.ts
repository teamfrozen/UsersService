import supertest, {SuperAgentTest} from 'supertest';
import {INestApplication} from '@nestjs/common';
import {addMiddlewares} from '../main';
import {AppModule} from '../app.module';
import {Test} from '@nestjs/testing';

export interface TestCtx {
  app: INestApplication;
  agent: SuperAgentTest;
}

export async function setup(): Promise<TestCtx> {
  const appModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  const app = appModule.createNestApplication();
  addMiddlewares(app);

  await app.init();

  const agent = supertest.agent(app.getHttpServer());

  return {app, agent};
}
