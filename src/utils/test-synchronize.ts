import {INestApplication} from '@nestjs/common';
import {Connection} from 'typeorm';

export async function synchronize(app: INestApplication | undefined): Promise<void> {
  if (app) {
    const connection = app.get(Connection);
    await connection.synchronize(true);
  }
}
