export function replaceParam(string: string, param: string) {
  return string.replace(/:\w+/, param);
}
