export interface MaskObjectConfig {
  // all - маскирует значения на любом уровне вложенности, пример: 'id' - маскирует все значения с ключем 'id' в объекте
  // null - маскирует только конкретные свойства в объекте, пример: 'payload.value.key'
  levels: 'all' | null;
  stab?: string;
}

/**
 * @See https://jira.tcsbank.ru/browse/SP-4188
 * @description Рекурсивно заменяет значения ключей объекта из массива keys на stab
 * @param value - объект с приватными данными
 * @param keys - массив полей, которые нуждаются в маскировании
 * @param config - конфигурация маскирования
 * @returns - объект с маскированными полями
 */
export const maskObject = <T extends unknown>(
  value: T,
  keys: string[],
  config?: MaskObjectConfig,
): T => {
  const {levels = null, stab = '*'} = config || {};

  // создаем WeakSet, чтобы избежать вечного цикла
  // при получении объекта с рекурсивный ссылкой на самого себя
  // @See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
  const seen = new WeakSet();

  const startOmit = (value: any, key?: string): any => {
    const isCyclic = seen.has(value);
    if (isCyclic) {
      return undefined;
    }

    if (isPrivateKey(keys, key)) {
      return stab;
    }

    if (isDateOrDateString(value)) {
      return value;
    }

    if (isArray(value)) {
      return value.map((v) => startOmit(v, key));
    }

    if (isObject(value)) {
      seen.add(value);

      if (levels === 'all') {
        return mapObjectValues(value, startOmit);
      }

      return mapObjectValues(value, (value, deepKey) => {
        return startOmit(value, buildObjectKey(key, deepKey));
      });
    }

    value = maskJson(value, keys, config);

    return maskUrlQueryString(value, keys, {stab});
  };

  return startOmit(value);
};

function isPrivateKey(keys: string[], key = '') {
  return keys.includes(key);
}

function buildObjectKey(prevKey?: string, nextKey?: string) {
  if (!nextKey && !prevKey) {
    return '';
  } else if (!prevKey) {
    return nextKey;
  } else if (!nextKey) {
    return prevKey;
  } else {
    return prevKey + '.' + nextKey;
  }
}

function maskJson(value: string, keys: string[], config?: MaskObjectConfig) {
  // проверяем что строка является json-ом, который содержит данные для маскирования
  // пример: {\"field\": \"sensitive\"}
  try {
    const json = JSON.parse(value);
    return JSON.stringify(maskObject(json, keys, config));
  } catch {}

  return value;
}

export interface MaskQueryStringConfig {
  stab?: string;
}

/**
 * @See https://jira.tcsbank.ru/browse/SP-4188
 * @description Маскирует параметры в Query String для переданной строки
 * @param value - строка URL c Query String
 * @param keys - массив ключей для фильтрации значений
 * @param config - конфигурация маскирования
 * @returns string (URL)
 */
export function maskUrlQueryString(
  value: string,
  keys: string[],
  config?: MaskQueryStringConfig,
): string {
  const {stab = '*'} = config || {};

  try {
    // проверяем что value является URL, например: http://localhost:3000
    const url = new URL(value);
    if (url.search) {
      url.search = maskURLSearchParams(url.search, keys, stab);
    }

    return url.toString();
  } catch {}

  return value;
}

function maskURLSearchParams(queryString: string, keys: string[], stab: string): string {
  const urlSearchParams = new URLSearchParams(queryString);

  keys.forEach((key) => urlSearchParams.has(key) && urlSearchParams.set(key, stab));

  return urlSearchParams.toString();
}

export function isArray<T>(value: any): value is T[] {
  return Array.isArray(value);
}

export function isDateOrDateString(value: any): boolean {
  return value instanceof Date || !isNaN(Number(new Date(value)));
}

export function isObject(value: any): boolean {
  return value instanceof Object;
}

export function mapObjectValues<T>(
  value: Record<string, T>,
  callback: (value: T, key?: string) => T,
): Record<string, T> {
  return Object.entries(value).reduce((obj, [objectKey, objectValue]) => {
    obj[objectKey] = callback(objectValue, objectKey);
    return obj;
  }, {} as Record<string, T>);
}

export function isValidNumber(number: number): boolean {
  return (
    typeof number === 'number' &&
    !Number.isNaN(Number(number)) &&
    number !== Infinity &&
    number !== -Infinity
  );
}
