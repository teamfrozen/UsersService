import {Module} from '@nestjs/common';
import {TypeOrmModule, TypeOrmModuleOptions} from '@nestjs/typeorm';
import {DbConfig} from '../config/db-config';

export const dbConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: DbConfig.POSTGRES_HOST,
  database: DbConfig.POSTGRES_DB,
  username: DbConfig.POSTGRES_USER,
  password: DbConfig.POSTGRES_PASSWORD,
  port: DbConfig.POSTGRES_PORT,
  entities: [`${__dirname}/../**/*.entity.{ts,js}`],
  migrations: [`${__dirname}/../../database/migrations/*.{ts,js}`],
  cli: {migrationsDir: `${__dirname}/../../database/migrations`},
};

@Module({
  imports: [TypeOrmModule.forRoot(dbConfig)],
})
export class DatabaseModule {}
