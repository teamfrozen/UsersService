import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {version, description, name} from '../package.json';
import {AppConfig} from './config/app-config';
import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {Logger} from 'nestjs-pino';
import {ErrorFilter} from './common/error-filter';
import {INestApplication} from '@nestjs/common';
import {AuthStrategy} from './auth/auth.constants';

startApp(AppConfig.APP_PORT);

async function startApp(port: number) {
  const app = await NestFactory.create(AppModule, {abortOnError: false});

  addMiddlewares(app);
  setupSwagger(app);

  await app.listen(port);
}

export function addMiddlewares(app: INestApplication) {
  const logger = app.get(Logger);

  app.useGlobalFilters(new ErrorFilter(logger));

  if (process.env.NODE_ENV !== 'test') {
    app.useLogger(logger);
  }
}

function setupSwagger(app: INestApplication): void {
  if (AppConfig.APP_SWAGGER) {
    const swaggerDocOptions = new DocumentBuilder()
      .addApiKey(
        {
          type: 'apiKey' as const,
          name: AuthStrategy.ApiKey,
          in: 'header',
        },
        AuthStrategy.ApiKey,
      )
      .addBearerAuth()
      .setDescription(description)
      .setVersion(version)
      .setTitle(name)
      .build();

    const swaggerDoc = SwaggerModule.createDocument(app, swaggerDocOptions);

    SwaggerModule.setup('docs', app, swaggerDoc, {
      customSiteTitle: `Swagger — ${name}`,
      customfavIcon: '',
      swaggerOptions: {
        defaultModelsExpandDepth: -1,
        persistAuthorization: true,
        syntaxHighlight: {theme: 'nord'},
      },
    });
  }
}
