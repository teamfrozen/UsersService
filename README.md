# Users Service

## Getting started

```bash
# create network to connect service and db
$ docker network create hse
# start service
$ docker-compose up
```

## Swagger

Swagger is enabled on `localhost:3000/docs`
